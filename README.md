# How to use?
Clone this repository using `git(1)` command:

```
git clone https://gitlab.com/sincorchetes/Twister.git
```

Do execute perms to script:
```
chmod +x twister
```

Execute!
```
./twister
```
